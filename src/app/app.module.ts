import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MaterialModule} from '@angular/material';
import { AUTH_PROVIDERS, JwtHelper } from 'angular2-jwt';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';

import { DatePickerModule } from 'ng2-datepicker';
import {CalendarModule} from 'primeng/primeng';

import { NoContentComponent } from './no-content';
import { SpinnerComponent } from './base/spinner';

import { LandpageComponent } from './landpage/base';
import { HomeComponent } from './landpage/home';
import { LoginComponent } from './landpage/login'; 

import { DashboardComponent } from './dashboard/base';
import { FarmsComponent } from './dashboard/farms';
import { FarmComponent } from './dashboard/farm';
import { SeasonsComponent } from './dashboard/seasons';
import { SeasonComponent } from './dashboard/season';
import { DefaultUserInfoComponent } from './dashboard/defaultuserinfo';

import { ApiService } from './services/api.service';
import { AuthService } from './services/auth.service';
import { RefereshTokenService } from './services/refreshToken.service';

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,     
    NoContentComponent,
    SpinnerComponent,
    LandpageComponent,
    HomeComponent,
    LoginComponent,
    DashboardComponent,
    FarmsComponent,
    FarmComponent,
    SeasonsComponent,
    SeasonComponent,
    DefaultUserInfoComponent
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules }),
    ReactiveFormsModule,
    DatePickerModule,
    CalendarModule
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    APP_PROVIDERS,
    AUTH_PROVIDERS,
    JwtHelper,
    RefereshTokenService,
    AuthService,
    ApiService]
  })
export class AppModule {
  constructor(public appRef: ApplicationRef,
              public appState: AppState,
              private refereshTokenService:RefereshTokenService) {

    this.refereshTokenService.startupTokenRefresh();

              }

  hmrOnInit(store: StoreType) {
    if (!store || !store.state) return;
    console.log('HMR store', JSON.stringify(store, null, 2));
    // set state
    this.appState._state = store.state;
    // set input values
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();

    delete store.state;
    delete store.restoreInputValues;
  }

  hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // save state
    const state = this.appState._state;
    store.state = state;
    // recreate root elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    store.restoreInputValues  = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  hmrAfterDestroy(store: StoreType) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }

}

