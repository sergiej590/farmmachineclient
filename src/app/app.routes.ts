import { Routes, RouterModule } from '@angular/router';

import { NoContentComponent } from './no-content';
import { DataResolver } from './app.resolver';

import { LandpageComponent } from './landpage/base';
import { HomeComponent } from './landpage/home';
import { LoginComponent } from './landpage/login';

import { DashboardComponent } from './dashboard/base';
import { FarmsComponent } from './dashboard/farms';
import { FarmComponent } from './dashboard/farm';

import { SeasonsComponent } from './dashboard/seasons';
import { SeasonComponent } from './dashboard/season';

import { DefaultUserInfoComponent } from './dashboard/defaultuserinfo';



export const ROUTES: Routes = [
  { path: '', component: LandpageComponent,    
    children: [{ path: '', component: HomeComponent},
               { path: 'login', component: LoginComponent}]},
  { path: 'dashboard',     component: DashboardComponent,
    children: [{ path: 'farms', component: FarmsComponent},
               { path: 'farm', component: FarmComponent},
               { path: 'farm/:id', component: FarmComponent},
               { path: 'seasons', component: SeasonsComponent},
               { path: 'season', component: SeasonComponent},
               { path: 'season/:id', component: SeasonComponent},
               { path: 'defaultuserinfo', component: DefaultUserInfoComponent}]},
  { path: '**',    component: NoContentComponent },
];
