import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import 'rxjs/Rx';
import { FarmService } from '../../services/farm.service'

import {BaseComponent} from '../../base/basecomponent';  

declare var jQuery:any;

@Component({
  selector: 'farm', 
  templateUrl: './farm.component.html',
  providers:[FarmService]
})
export class FarmComponent extends BaseComponent{
    createFarmForm: FormGroup;
    submitClicked:Boolean = false;
    requestCount=0;

    farmCategories:any = [];
    farm:any;
    id:string = null;
    
    get formCategories(): FormArray { return this.createFarmForm.get('categories') as FormArray; }
    get formAddresses(): FormArray { return this.createFarmForm.get('addresses') as FormArray; }

    constructor(private fb: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                private farmService:FarmService){
        super();
    }

    ngOnInit(){
        this.initState();

        this.startRequesting();
        this.farmService.GetFarmCategories()
            .subscribe(
                data => {
                            this.farmCategories = data;
                            this.addCategories(this.farmCategories, null);
                            this.checkRequest();
                        },
                err => this.failResult("CATEGORIES_REQUEST_ERROR"),
                () => {
                    if(this.id == null)
                    {
                        this.stopRequesting()
                }});

         this.route.params
                                .map(params => params['id'])
                                .subscribe((id) => {
                                    this.id = id;
                                    if(this.id != null)
                                    {
                                        this.startRequesting();
                                        this.farmService.GetFarm(this.id)
                                            .subscribe(
                                                data => {
                                                            this.farm = data;
                                                            this.checkRequest();
                                                        },
                                                err => this.failResult("FARM_NOT_FOUND"),
                                                () => this.stopRequesting());
                                    }
                                });
    }

    checkRequest()
    {
        this.requestCount++;
        if(this.requestCount == 2)
        {
            this.fillData(this.farm);
        }
    }

    initState()
    {
        this.submitClicked = false;
        this.requestError = false;
        this.requestSuccess = false;
        this.requestErrorCode = null;
        this.createForm();
        this.addAddress(null, true);
    }

    createForm()
    {
            this.createFarmForm = this.fb.group({
                    identifier: ["", Validators.required],
                    name: ["", Validators.required],
                    active: [true],
                    categories: this.fb.array([]),
                    addresses: this.fb.array([])
            });
    }

    initCategory(id, name, checked) {
        return this.fb.group({
            id: [id, Validators.required],
            name: [name, Validators.required],
            checked: [checked]
        });
    }

    addCategories(data, userData) {  
         for(var i in data)
         {    
            var category = data[i];
            
            var checked = false;
            if(userData != null)
            {
                for(var j in userData)
                {
                    if(userData[j].id == category.id)
                    {
                        checked = true;
                    }
                }
            }
            
            const categoryCtrl = this.initCategory(category.id, category.name, checked);        
            this.formCategories.push(categoryCtrl);
        }
    }

    initAddress(id,country, city, street, postCode, homeNo, main) {
        return this.fb.group({
            id: [id],
            country: [country, Validators.required],
            city: [city, Validators.required],
            street: [street, Validators.required],
            postCode: [postCode, Validators.required],
            homeNo: [homeNo],
            main: [main]
        });
    }

    addAddress(address, main) {      
        var addrCtrl = null;
        
        if(address == null)
        {
            addrCtrl = this.initAddress('','Polska','','','','', main);    
        }    
        else
        {
            addrCtrl = this.initAddress(address.id, address.country, address.city, address.street,
                                        address.postCode, address.homeNo, address.main)
        }

        this.formAddresses.push(addrCtrl);
    }

    removeAddress(i: number) {
        this.formAddresses.removeAt(i);
    }

    fillData(farm)
    {
        this.createForm();

        this.id = farm.id;
        this.createFarmForm.get('name').setValue(farm.name);
        this.createFarmForm.get('identifier').setValue(farm.identifier);
        this.createFarmForm.get('active').setValue(farm.active);

        for(var i in farm.addresses)
        {
            var address = farm.addresses[i];
            this.addAddress(address, false);
        }

        this.addCategories(this.farmCategories, farm.categories);
    }

    save(event)
    {
        this.submitClicked = true;

        if(this.createFarmForm.valid)
        {
            if(this.id == null)
            {
                this.startRequesting();
                this.farmService.CreateFarm(this.createFarmForm.value)
                    .subscribe(
                        data => 
                        {
                            this.fillData(data);
                            this.successResult()
                        },
                        err =>  this.failResult(err._body),
                        () => this.stopRequesting());
            }
            else
            {
                this.startRequesting();
                this.farmService.UpdateFarm(this.id, this.createFarmForm.value)
                    .subscribe(
                        data => 
                        {
                            this.fillData(data);
                            this.successResult();
                        },
                        err =>  this.failResult(err._body),
                        () => this.stopRequesting());
            }
        }
        event.preventDefault();
    }

    delete()
    {
        this.startRequesting();
        this.farmService.DeleteFarm(this.id)
                    .subscribe(
                        data => this.router.navigate(['/dashboard/farms']),
                        err => this.failResult(err._body),
                        () => this.stopRequesting());
    }
}
