import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import 'rxjs/Rx';
import { FarmService } from '../../services/farm.service'
import { SeasonService } from '../../services/season.service'
import { DefaultUserInfoService } from '../../services/defaultuserinfo.service'


import {BaseComponent} from '../../base/basecomponent';  

declare var jQuery:any;

@Component({
  selector: 'defaultuserinfo', 
  templateUrl: './defaultuserinfo.component.html',
  providers:[FarmService, SeasonService, DefaultUserInfoService]
})
export class DefaultUserInfoComponent extends BaseComponent{
    updateDefaultUserInfoForm: FormGroup;
    submitClicked:Boolean = false;
    requestErrorCode:any = null;
    requestSuccess:Boolean = false;
    requestError:Boolean = false;
    
    id:string = null;
    farms:any = [];
    seasons:any = [];
    defaultUserInfo:any;
    requestCount=0;

    constructor(private fb: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                private farmService:FarmService,
                private seasonService:SeasonService,
                private defaultUserInfoService:DefaultUserInfoService){
        super();
    }

    ngOnInit(){
        this.initState();

        this.startRequesting();
        this.farmService.GetFarms()
            .subscribe(
                data => {
                            this.farms = data;
                            this.checkRequest();
                        },
                err => this.failResult("FARMS_REQUEST_ERROR"),
                () => {
                        this.stopRequesting()
                });

        this.seasonService.GetSeasons(null,null,1,1000)
            .subscribe(
                data => {
                            this.seasons = data.items; 
                            this.seasons.sort((a, b) => {
                                                    if (a.creationTime > b.creationTime) return -1;
                                                    else if (a.creationTime < b.creationTime) return 1;
                                                    else return 0;
                                                });
                            this.checkRequest();
                        },
                err => this.failResult("SEASONS_REQUEST_ERROR"),
                () => {
                        this.stopRequesting()
                    });

        this.defaultUserInfoService.GetDefaultUserInfo()
            .subscribe(
                data => {
                            this.defaultUserInfo = data;   
                            this.checkRequest();
                        },
                err => this.failResult("DEFAULT_USER_INFO_REQUEST_ERROR"),
                () => {
                        this.stopRequesting()
                    });
    }

    checkRequest()
    {
        this.requestCount++;
        if(this.requestCount == 3)
        {
            this.fillData(this.defaultUserInfo);
        }
    }

    initState()
    {
        this.submitClicked = false;
        this.requestError = false;
        this.requestSuccess = false;
        this.requestErrorCode = null;
        this.createForm();
    }

    createForm()
    {
            this.updateDefaultUserInfoForm = this.fb.group({
                    seasonId: [""],
                    farmId: [""],
            });
    }  

    fillData(data)
    {
        this.createForm();

        this.updateDefaultUserInfoForm.get('seasonId').setValue(data.seasonId);
        this.updateDefaultUserInfoForm.get('farmId').setValue(data.farmId);
    }

    save(event)
    {
        this.submitClicked = true;

        if(this.updateDefaultUserInfoForm.valid)
        {
                this.startRequesting();
                this.defaultUserInfoService.UpdateDefaultUserInfo(this.updateDefaultUserInfoForm.value)
                    .subscribe(
                        data => 
                        {
                            this.fillData(data);
                            this.successResult();
                        },
                        err =>  this.failResult(err._body),
                        () => this.stopRequesting());
        }
        event.preventDefault();
    }
}
