import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FarmService } from '../../services/farm.service'


import {BaseComponent} from '../../base/basecomponent';  

@Component({
  selector: 'farms', 
  templateUrl: './farms.component.html',
  providers:[FarmService]
})
export class FarmsComponent extends BaseComponent {
    farms = [];

    constructor(private farmService:FarmService){
        super();
    }

    ngOnInit(){
        this.startRequesting();
        this.farmService.GetFarms()
            .subscribe(
            data => {this.farms = data;
                     this.farms.sort((a, b) => {
                                                    if (a.creationTime > b.creationTime) return -1;
                                                    else if (a.creationTime < b.creationTime) return 1;
                                                    else return 0;
                                                });
            },
            err => 
            {
                console.log(err);
                this.stopRequesting();
            },
            () => this.stopRequesting());
    }
}
