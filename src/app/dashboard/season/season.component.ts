import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import 'rxjs/Rx';
import { SeasonService } from '../../services/season.service'
import { FarmService } from '../../services/farm.service'

import {BaseComponent} from '../../base/basecomponent';  

declare var jQuery:any;

@Component({
  selector: 'season', 
  templateUrl: './season.component.html',
  providers:[SeasonService, FarmService]
})
export class SeasonComponent extends BaseComponent{
    createSeasonForm: FormGroup;
    submitClicked:Boolean = false;
    requestCount=0;

    farms:any = [];
    season:any;
    id:string = null;
    
    get formFarms(): FormArray { return this.createSeasonForm.get('farms') as FormArray; }

    constructor(private fb: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                private seasonService:SeasonService,
                private farmService:FarmService){
        super();
    }

    ngOnInit(){

        this.initState();

        this.startRequesting();
        this.farmService.GetFarms()
            .subscribe(
                data => {
                            this.farms = data;
                            this.addFarms(this.farms, null);
                            this.checkRequest();
                        },
                err => this.failResult("FARMS_REQUEST_ERROR"),
                () => {
                    if(this.id == null)
                    {
                        this.stopRequesting()
                }});

         this.route.params
                                .map(params => params['id'])
                                .subscribe((id) => {
                                    this.id = id;
                                    if(this.id != null)
                                    {
                                        this.startRequesting();
                                        this.seasonService.GetSeason(this.id)
                                            .subscribe(
                                                data => {
                                                            this.season = data;
                                                            this.checkRequest();
                                                        },
                                                err => this.failResult("SEASON_NOT_FOUND"),
                                                () => this.stopRequesting());
                                    }
                                });
    }

    checkRequest()
    {
        this.requestCount++;
        if(this.requestCount == 2)
        {
            this.fillData(this.season);
        }
    }

    initState()
    {
        this.submitClicked = false;
        this.requestError = false;
        this.requestSuccess = false;
        this.requestErrorCode = null;
        this.createForm();
    }

    createForm()
    {
            this.createSeasonForm = this.fb.group({
                    dateFrom: ['', [Validators.required, Validators.pattern('^[0-9]{4}-[0-9]{2}-[0-9]{2}$')]],
                    dateTo: ['', [Validators.required, Validators.pattern('^[0-9]{4}-[0-9]{2}-[0-9]{2}$')]],
                    farms: this.fb.array([]),
            });
    }

    initFarm(id, name, checked) {
        return this.fb.group({
            id: [id, Validators.required],
            name: [name, Validators.required],
            checked: [checked]
        });
    }

    addFarms(data, userData) {  
         for(var i in data)
         {    
            var farm = data[i];
            
            var checked = false;
            if(userData != null)
            {
                for(var j in userData)
                {
                    if(userData[j].id == farm.id )
                    {
                        checked = true;
                    }
                }
            }
            
            const farmCtrl = this.initFarm(farm.id, farm.name, checked);        
            this.formFarms.push(farmCtrl);
        }
    }

    fillData(season)
    {
        this.createForm();

        this.id = season.id;
        this.createSeasonForm.get('dateFrom').setValue(season.dateFrom.substring(0,10));
        this.createSeasonForm.get('dateTo').setValue(season.dateTo.substring(0,10));

        this.addFarms(this.farms, season.farms);
    }

    save(event)
    {
        this.submitClicked = true;

        if(this.createSeasonForm.valid)
        {
            if(this.id == null)
            {
                this.startRequesting();
                this.seasonService.CreateSeason(this.createSeasonForm.value)
                    .subscribe(
                        data => 
                        {
                            this.fillData(data);
                            this.successResult();
                        },
                        err =>  this.failResult(err._body),
                        () => this.stopRequesting());
            }
            else
            {
                this.startRequesting();
                this.seasonService.UpdateSeason(this.id, this.createSeasonForm.value)
                    .subscribe(
                        data => 
                        {
                            this.fillData(data);
                            this.successResult();
                        },
                        err =>  this.failResult(err._body),
                        () => this.stopRequesting());
            }
        }
        event.preventDefault();
    }

    delete()
    {
        this.startRequesting();
        this.seasonService.DeleteSeason(this.id)
                    .subscribe(
                        data => this.router.navigate(['/dashboard/seasons']),
                        err => this.failResult(err._body),
                        () => this.stopRequesting());
    }

   
}
