import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SeasonService } from '../../services/season.service'

import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';


import {BaseComponent} from '../../base/basecomponent';  

declare var jQuery: any;

@Component({
  selector: 'seasons', 
  templateUrl: './seasons.component.html',
  styleUrls: ['./seasons.component.css'],
  providers: [SeasonService]
})
export class SeasonsComponent extends BaseComponent {
    seasons =[];
    searchForm: FormGroup;

    constructor(private fb:FormBuilder,
                private seasonService:SeasonService){
        super();

        this.searchForm = this.fb.group({
            dateFrom: ['', Validators.pattern('^[0-9]{4}-[0-9]{2}-[0-9]{2}$')],
            dateTo: ['', Validators.pattern('^[0-9]{4}-[0-9]{2}-[0-9]{2}$')],
            });
    }

    ngOnInit(){
        this.getSeasons();

         this.searchForm.valueChanges.subscribe(value => {
                if(this.searchForm.valid)
                {
                    this.getSeasons();
                }
             });
    }

    setPage(page)
    {
        this.changeCurrentPage(page);
        this.getSeasons();
    }

    getSeasons()
    {        
        this.startRequesting();
        this.seasonService.GetSeasons(this.searchForm.get('dateFrom').value,
                                      this.searchForm.get('dateTo').value, 
                                      this.pager.currentPage,
                                      this.pager.pageSize)
            .subscribe(
            data => {this.seasons = data.items;

                    this.pager.totalRows = data.totalRows;
                    this.preparePages();

                    this.seasons.sort((a, b) => {
                                                    if (a.creationTime > b.creationTime) return -1;
                                                    else if (a.creationTime < b.creationTime) return 1;
                                                    else return 0;
                                                });
            },
            err => 
            {
                console.log(err);
                this.stopRequesting();
            },
            () => this.stopRequesting());
    }
}
