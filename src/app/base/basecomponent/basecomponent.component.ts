import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

declare var jQuery:any;

@Component({
selector: 'base-component'})

export class BaseComponent {
  
    public isRequesting: boolean;
    
    requestErrorCode:any = null;
    requestSuccess:Boolean = false;
    requestError:Boolean = false;

    public pager ={
         pageSize : 10,
         pageSizes : [5,10,25],
         pages :[],        
         currentPage : 1,
         totalRows : 0
    };
  
  constructor()
  {
  }

  public startRequesting()
  {
      this.isRequesting = true;
  }

  public stopRequesting()
  {
      this.isRequesting = false;
  }

  public preparePages()
  {
      this.pager.pages = [];

      var pageCount =  (this.pager.totalRows / this.pager.pageSize);
      if(this.pager.totalRows % this.pager.pageSize > 0)
      {
          pageCount++;
      } 
      
      for(var i = 1; i <= pageCount; i++){
            this.pager.pages.push(i);
      }
  }

  public changeCurrentPage(page)
  {
      this.pager.currentPage = page;
  }

   successResult()
    {
        this.requestSuccess = true;
        this.requestError = false;
        this.scrollTo("#request_success_section");
    }

    failResult(error)
    {
        this.requestSuccess = false;
        this.requestError = true;
        this.requestErrorCode = error;
        this.scrollTo("#request_error_section");
        this.stopRequesting();
    }

    scrollTo(element)
    {
        jQuery('html, body').animate({
                                        scrollTop: jQuery(element).offset().top
                                    }, 1000);
    }
}
