import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { ApiService } from './api.service';
import 'rxjs/Rx';

@Injectable()
export class SeasonService {
  
  route = "api/seasons";

  constructor(private api : ApiService) {
  }
  
  public GetSeasons(dateFrom, dateTo, page, pageSize)
  {        
      var params = "?dateFrom=" + dateFrom + "&dateTo=" + dateTo + "&page=" + page + "&pageSize=" + pageSize;
      return this.api.Get(this.route + params);
  } 

  public CreateSeason(season)
  {
      return this.api.Post(this.route, season);
  }

  public GetSeason(id)
  {        
      return this.api.Get(this.route + '/' + id);
  } 

  public UpdateSeason(id, season)
  {
      return this.api.Put(this.route + '/' + id, season);
  }

  public DeleteSeason(id)
  {
      return this.api.Delete(this.route + '/' + id);
  }
}
