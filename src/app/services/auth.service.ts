import { Injectable }      from '@angular/core';
import { tokenNotExpired} from 'angular2-jwt';
import {Observable} from 'rxjs/Rx';
import { ApiService} from './api.service';
import { RefereshTokenService } from './refreshToken.service';

 declare const FB:any;

@Injectable() 
export class AuthService {

  constructor(private api:ApiService,
              private refreshToken:RefereshTokenService) {
  }
      
  public login(username:string, password:string) {
      let resource = 'connect/token';
      let body = "grant_type=password&username=" + username +
                 "&password=" + password +
                 "&scope=offline_access";
      
      var observable = this.api.PostForm(resource, body)
                               .share();

      observable.subscribe(
            data => { 
                      localStorage.setItem("id_token", data.access_token);
                      localStorage.setItem("refresh_token", data.refresh_token);
                      this.refreshToken.scheduleRefresh();
                    });

      return observable;
  };  

   public loginExternal(fbToken: string)
    {
        let resource = 'connect/token';
        let body = "grant_type=urn:ietf:params:oauth:grant-type:facebook_identity_token" +
                  "&assertion=" + fbToken +
                  "&scope=offline_access";
        
        var observable = this.api.PostForm(resource, body)
                                 .share();
                                
        observable.subscribe(
              data => { 
                        localStorage.setItem("id_token", data.access_token);
                        localStorage.setItem("refresh_token", data.refresh_token);
                        this.refreshToken.scheduleRefresh();
                      });

        return observable;
    }

    public authenticated() {
        return tokenNotExpired();
      }; 

      public logout() {
          localStorage.removeItem('id_token');
          localStorage.removeItem('refresh_token');
          this.refreshToken.unscheduleRefresh();
      };
}
