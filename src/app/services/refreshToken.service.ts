import { Injectable }      from '@angular/core';
import { tokenNotExpired, JwtHelper, AuthHttp} from 'angular2-jwt';
import {Observable} from 'rxjs/Rx';
import { ApiService} from './api.service';

@Injectable() 
export class RefereshTokenService {
    
  refreshSubscription: any;

    constructor(private jwtHelper:JwtHelper,
                private authHttp:AuthHttp,
                private api:ApiService){

    }
 

  public scheduleRefresh() {
    // If the user is authenticated, use the token stream
    // provided by angular2-jwt and flatMap the token
    let source = this.authHttp.tokenStream.flatMap(
      token => {
        // The delay to generate in this case is the difference
        // between the expiry time and the issued at time
        let jwtIat = this.jwtHelper.decodeToken(token).iat;
        let jwtExp = this.jwtHelper.decodeToken(token).exp;
        let iat = new Date(0);
        let exp = new Date(0);

        let delay = (exp.setUTCSeconds(jwtExp) - iat.setUTCSeconds(jwtIat));

        return Observable.interval(delay);
      });

    this.refreshSubscription = source.subscribe(() => {
      this.getNewJwt();
    });
  }

  public startupTokenRefresh() {
    // If the user is authenticated, use the token stream
    // provided by angular2-jwt and flatMap the token
    var rt =localStorage.getItem('refresh_token');
    var it =localStorage.getItem('id_token');

 //   if (tokenNotExpired()) {
   if(rt != null && it != null)
   {
      let source = this.authHttp.tokenStream.flatMap(
        token => {
          // Get the expiry time to generate
          // a delay in milliseconds
          let now: number = new Date().valueOf();
          let jwtExp: number = this.jwtHelper.decodeToken(token).exp;
          let exp: Date = new Date(0);
          exp.setUTCSeconds(jwtExp);
          let delay: number = exp.valueOf() - now;

          // Use the delay in a timer to
          // run the refresh at the proper time
          return Observable.timer(delay);
        });

       // Once the delay time from above is
       // reached, get a new JWT and schedule
       // additional refreshes
       source.subscribe(() => {
         this.getNewJwt();
         this.scheduleRefresh();
       });
    }
    else
    {
        console.log("Refresh or access token does not exist");
    }
  }

  public unscheduleRefresh() {
    // Unsubscribe fromt the refresh
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  private getNewJwt()
  {
      var refreshToken = localStorage.getItem("refresh_token");

      if(refreshToken != null)
      {
        let resource = 'connect/token';
        let body = "grant_type=refresh_token" +
                  "&refresh_token=" + refreshToken + 
                  "&scope=offline_access";
        
        this.api.PostForm(resource, body)
            .subscribe(
              data => { 
                        localStorage.setItem("id_token", data.access_token);
                        localStorage.setItem("refresh_token", data.refresh_token )
                      },
              err => console.log('Error during refreshing token'));
      }
      else
      {
          console.log("Refresh token does not exist");
      }
  }
}