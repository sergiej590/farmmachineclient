import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { ApiService } from './api.service';
import 'rxjs/Rx';

@Injectable()
export class FarmService {
  
  route = "api/farms";

  constructor(private api : ApiService) {
  }
  
  public GetFarms()
  {        
      return this.api.Get(this.route);
  } 

  public GetFarmCategories()
  {        
      return this.api.Get(this.route + '/categories');
  } 

  public GetFarmAddressCategories()
  {        
      return this.api.Get(this.route + '/address/categories');
  } 

  public CreateFarm(farm)
  {
      return this.api.Post(this.route, farm);
  }

  public GetFarm(id)
  {        
      return this.api.Get(this.route + '/' + id);
  } 

  public UpdateFarm(id, farm)
  {
      return this.api.Put(this.route + '/' + id, farm);
  }

  public DeleteFarm(id)
  {
      return this.api.Delete(this.route + '/' + id);
  }
}
