import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { ApiService } from './api.service';
import 'rxjs/Rx';

@Injectable()
export class DefaultUserInfoService {
  
  route = "api/defaultuserinfos";

  constructor(private api : ApiService) {
  }
  
  public GetDefaultUserInfo()
  {        
      return this.api.Get(this.route);
  } 

  public UpdateDefaultUserInfo(defaultUserInfo)
  {
      return this.api.Put(this.route, defaultUserInfo);
  }
}
