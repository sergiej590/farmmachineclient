import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/Rx';

@Injectable()
export class ApiService {
  
  apiUrl = "http://localhost:50417/";

  constructor(private http : Http,
              private authHttp : AuthHttp) {
  }
  
  public PostForm(route:string, body:string)
  {        
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
      let options = new RequestOptions({ headers: headers, method: "post" });
      
      return this.http.post(this.apiUrl + route, body, options)     
                      .map((res:Response) => res.json());
  }
  
  public Get(route:string)
  {       
      return this.authHttp.get(this.apiUrl + route)
                          .map((res:Response) => res.json());
  } 

  public Post(route:string, model:any)
  {
      return this.authHttp.post(this.apiUrl + route, model)
                          .map((res:Response) => res.json());
  }

  public Put(route:string, model:any)
  {
      return this.authHttp.put(this.apiUrl + route, model)
                          .map((res:Response) => res.json());
  }

  public Delete(route:string)
  {
      return this.authHttp.delete(this.apiUrl + route);
  }
}
