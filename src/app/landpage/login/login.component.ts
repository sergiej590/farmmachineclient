import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {ApiService} from '../../services/api.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  providers : [AuthService, ApiService]
})
export class LoginComponent {

loginForm: FormGroup;

  constructor(fb: FormBuilder,
              private router: Router,
              private auth: AuthService,
              private api: ApiService) {
    this.loginForm = fb.group({
      email: ["", Validators.required],
      password: ["", Validators.required]
    });
  }
  
  doLogin(event) {
    this.auth.login(this.loginForm.value.email, this.loginForm.value.password)
             .subscribe(data => this.router.navigate(['dashboard']),
                        err => console.log("login failed"));

    event.preventDefault();
  }

  logout()
  {
    this.auth.logout();
  }

  sendGet()
  {        
    var result =this.api.Get('api/values')
    result.subscribe(
              data =>  console.log(data),
              err => alert("eror get"),
              () => console.log('Request Complete')
            );
  }

}   


