import { Component } from '@angular/core';
import {RefereshTokenService} from '../../services/refreshToken.service';
import {AuthService} from '../../services/auth.service';
import 'rxjs/Rx';
import {Router} from '@angular/router';

 declare const FB:any;

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'home'
  selector: 'landpage',  // <home></home>
  // Our list of styles in our component. We may add more to compose many styles together
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  templateUrl: './landpage.component.html'
})

export class LandpageComponent {
 
    constructor(private refreshToken: RefereshTokenService,
                private auth: AuthService,                
                private router: Router){
   }

    ngOnInit()
    {
      FB.init({
        appId      : '232059677220701',
        cookie     : false, 
        xfbml      : true,  
        version    : 'v2.7' 
      });
    } 

    public fbLogin()
    {
      FB.getLoginStatus(response => {
        this.statusChangeCallback(response);
        });
    }

    statusChangeCallback(response: any) 
    {
        if (response.status === 'connected') {
            var accessToken = response.authResponse.accessToken;          
            this.auth.loginExternal(accessToken).subscribe(
              data => { this.router.navigate(['dashboard'])},
              err => { /*TODO register*/});
        } else {
            FB.login(function(response) {},
                    {scope: 'email'});
        }
    }
}
